import React from 'react';
import { NavLink } from 'react-router-dom';

const Page = ({ page, handleOnClick, currentCategory }) => (
    <li className="pagination__listItem">
        <NavLink
            className="pagination__link"
            activeClassName="pagination__link--current"
            data-page={page}
            onClick={handleOnClick}
            to={`/catalog/${currentCategory}/${page}`}
        >
            {page}
        </NavLink>
    </li>
);

export default Page;
