import React, { Component } from 'react';
import { Page } from './components';

class Pagination extends Component {
    generatePagination = () => {
        let pages = [];
        const { arrayLength, pageLength } = this.props;
        const pagesLength = Math.ceil(arrayLength / pageLength);
        for (let i = 0; i < pagesLength; i += 1) {
            pages.push(i + 1);
        }
        return pages;
    };

    render() {
        const pagination = this.generatePagination();
        const { currentCategory, handleOnClick } = this.props;
        if (pagination.length <= 1) return null;
        return (
            <ul className="pagination">
                {pagination.map(page => (
                    <Page
                        key={page}
                        page={page}
                        currentCategory={currentCategory}
                        handleOnClick={handleOnClick}
                    />
                ))}
            </ul>
        );
    }
}

export default Pagination;
